#!/bin/bash

# Script, implementación de "loop devices"
# Vemos que nos sale.... 

declare ruta
declare -i cantImgns=0
declare -i pesoImgns=0
declare partTabImgns="legacy"
declare dispositivo
declare rt_afinidad
declare rt_conflictos
declare es_valido
declare dd_input
declare dd_output
declare -i dd_count
declare flag_dd=false
declare flag_losetup=false
declare flag_fdisk=false
declare flag_help=false


ruta=$(dirname $(realpath $0))
# incluir lib
source $ruta/libsetloop.sh

whatusay=$*

while getopts ":n:t:p:f:e:Elh" seleccion $whatusay ; do
	# Inicialización por ciclo
	rt_afinidad=1
	rt_conflictos=0

	case $seleccion in 
		n)
			echo es n
			cantImgns=$OPTARG

			afinidad "t"
			conflictos "(p|f|e|E|l)"
			validar
			if $es_valido ; then 
				echo OK
			else
				echo NOT OK
			fi
			
			echo "Rta.: $cantImgns"
			;;
		t)
			echo es t
			pesoImgns=$OPTARG

			afinidad "n"
			conflictos "(p|f|e|E|l)"
			validar
			if $es_valido ; then 
				echo OK
			else
				echo NOT OK
			fi

			echo "Rta.: $pesoImgns"
			;;
		d)
			echo es d
			dispositivo=$OPTARG
			afinidad "(p|f|e)"
			conflictos "(n|t|E|l)"
			validar
			if $es_valido ; then 
				echo OK
			else
				echo NOT OK
			fi
			;;
		p)
			echo es p
			partTabImgns=$OPTARG
			conflictos "(n|t|f|e|E|l)"
			validar
			if $es_valido ; then 
				echo OK
				case $partTabImgns in
					"legacy")
						echo es MBR
						;;
					"efi")
						echo es GPT
						;;
					*)
						echo Error
				esac
			else
				echo NOT OK
			fi
			;;
		f)
			echo es f
			dispositivo=$OTPARG
			conflictos "(n|t|p|e|E|l)"
			validar 
			if es_valido ; then
				echo OK
			else
				echo NOT OK
			fi
			echo "Rta.: $dispositivo"
			;;
		e)
			echo es e
			dispositivo=$OPTARG
			conflictos "(n|t|p|f|E|l)"
			validar 
			if es_valido ; then
				echo OK
			else
				echo NOT OK
			fi
			echo "Rta.: $dispositivo"
			;;
		E)
			echo es E
			conflictos "(n|t|p|f|e|l)"
			validar 
			if es_valido ; then
				echo OK
			else
				echo NOT OK
			fi
			;;
		l)
			echo es l
			conflictos "(n|t|p|f|e|E)"
			validar 
			if es_valido ; then
				echo OK
			else
				echo NOT OK
			fi
			;;
		h)
			echo es h
			;;
		*)
			echo es cualquier cosa!
	esac
	
done

