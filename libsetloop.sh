# Hallo


function conflictos {
	# detectar parametros incompatibles

	echo "$whatusay" | egrep "\-$1" 2>&1 >/dev/null
	if [ $? -eq 0 ] ; then
		rt_conflictos=1
	else
		rt_conflictos=0
	fi
	return $rt_conflictos
}

function afinidad {
	# detectar parametros afines

	echo "$whatusay" | egrep "\-$1" 2>&1 >/dev/null
	que=$?
	if [ $que -eq 0 ] ; then
		rt_afinidad=1
	else
		rt_afinidad=0
	fi
	return $rt_afinidad
}

function validar {
	# detectar coherencia de argumentos

	if [ $rt_afinidad -eq 1 ] && [ $rt_conflictos -eq 0 ] ; then 
		es_valido=true
	else
		es_valido=false
	fi

}
